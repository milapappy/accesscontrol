# frozen_string_literal: true

Rails.application.routes.draw do
  resources :resource
  mount_devise_token_auth_for 'User', at: 'auth', controllers:
      {
          registrations: 'custom_user_registration',
          sessions: 'custom_user_login_',
          token_validations: 'token_validations'

      }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
