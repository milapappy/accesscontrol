# frozen_string_literal: true

class ResourcePolicy
  attr_reader :user, :resource

  def initialize(user, resource)
    @user = user
    @resource = resource
  end

  def create?
    user.is_student?
  end

  def update?
    user.is_student?
  end

  def destroy?
    user.is_student?
  end

end
