# frozen_string_literal: true

class Resource < ApplicationRecord
  resourcify
  validates :resource_name, :resource_description, presence: true

end
