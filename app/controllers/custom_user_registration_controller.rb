# frozen_string_literal: true

class CustomUserRegistrationController < DeviseTokenAuth::RegistrationsController
  def new
    super
  end

  def create
    super do |_resource|
      if @resource
        user = User.find(@resource.id)
        user.add_role :student
      end
    end
  end

  def update
    super
  end
end

# TODO: Try role param on register
