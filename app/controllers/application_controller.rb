# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Pundit
  include DeviseTokenAuth::Concerns::SetUserByToken
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  begin
    def authenticate_current_user
      head :unauthorized if get_current_user.nil?
    end

    def get_current_user
      if request.headers['access-token'].nil? ||
          request.headers['client'].nil? || request.headers['uid'].nil?
        return nil
      end

      def get_current_user
        if request.headers['access-token'].nil? || request.headers['client'].nil? || request.headers['uid'].nil?
          return nil
        end

        current_user = User.find_by(uid: request.headers['uid'])

        if current_user&.tokens&.key?(request.headers['client'])
          token = current_user.tokens[request.headers['client']]
          expiration_datetime = DateTime.strptime(token['expiry'].to_s, '%s')

          expiration_datetime > DateTime.now
          @current_user = current_user
        end

        @current_user
      end
    end
  end

  private

  def user_not_authorized
    render json: {message: 'Unauthorized Access'}, status: 401
  end
end
