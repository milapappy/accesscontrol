# frozen_string_literal: true

class ResourceController < ApplicationController
  before_action :authenticate_current_user

  def index
    @resource = Resource.all
    render json: @resource
  end

  def show
    @resource = Resource.find(params[:id])
    render json: @resource
  end

  def create
    @resource = Resource.new(create_resource_params)
    authorize @resource
    if @resource.save
      render json: @resource
    else
      render json: @resource.errors, status: 400
    end
  end

  def update
    @resource = Resource.find(params[:id])
    authorize @resource
    if @resource
      @resource.update(update_resource_params)
      render json: {message: 'Update Successful'}, status: 200
    else
      render json: @resource.errors, status: 400
    end
  end

  def destroy
    @resource = Resource.find(params[:id])
    authorize @resource
    if @resource
      @resource.destroy
      render json: {message: 'Delete Successful'}, status: 200
    else
      render json: @resource.errors, status: 400
    end
  end

  def create_resource_params
    params.require(:Resource).permit(:resource_name, :resource_description)
  end

  def update_resource_params
    params.require(:Resource).permit(:resource_description)
  end
end
