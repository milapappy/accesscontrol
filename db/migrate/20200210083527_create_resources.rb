class CreateResources < ActiveRecord::Migration[6.0]
  def change
    create_table :resources do |t|
      t.string :resource_name
      t.string :resource_description
      t.timestamps
    end
  end
end
